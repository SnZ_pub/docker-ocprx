# docker-ocprx

Dockerized version of [OpenConnect VPN client](https://gitlab.com/openconnect/openconnect) with additional options:
- RSA SecurID token generator [stoken](https://github.com/cernekee/stoken)
- OpenConnect proxy [ocproxy](https://github.com/cernekee/ocproxy)

## Usage

### Auto token generation
```
docker run --rm -it \
	--name=ocprx \
	-e VPN_HOST='vpn.tld.com' \
	-e VPN_USER='firstname.lastname@tld.com' \
	-e VPN_PASS='YOUR-SUPER-SECURE-PASS' \
	-e VPN_GROUP='GROUP_NAME' \
	-e STOKEN_HASH='<stoken export>' \
	-e STOKEN_PIN='1234' \
	-p 1080:1080 registry.gitlab.com/snz_pub/docker-ocprx
```

### Manual token generation
```
docker run --rm -it \
	--name=ocprx \
	-e VPN_HOST='vpn.tld.com' \
	-e VPN_USER='firstname.lastname@tld.com' \
	-e VPN_PASS='YOUR-SUPER-SECURE-PASS' \
	-e VPN_GROUP='GROUP_NAME' \
	-e STOKEN_CODE='<stoken>' \
	-p 1080:1080 registry.gitlab.com/snz_pub/docker-ocprx
```
