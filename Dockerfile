ARG REGISTRY="registry.gitlab.com"
ARG IMAGE="snz_prv/docker-base-alpine"
ARG BASE_VERSION="3.9"
ARG VERSION="0.1"

FROM ${REGISTRY}/${IMAGE}:${BASE_VERSION}

ARG VERSION
ARG REPO="http://dl-cdn.alpinelinux.org/alpine/edge/testing"

ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2

LABEL maintainer="SnZ <snz@snz.pw>" \
      version="${VERSION}"

WORKDIR /app

RUN apk add --no-cache \
        libevent \
    && apk add --no-cache --repository "${REPO}" \
        stoken \
        openconnect \
    && apk add --no-cache --virtual=.build-deps \
        git \
        gcc \
        g++ \
        make \
        zlib-dev \
        autoconf \
        automake \
        libevent-dev \
        linux-headers \
        bsd-compat-headers \
    && git clone --depth=1 https://github.com/cernekee/ocproxy.git . \
    && ./autogen.sh && ./configure && make && make install \
    && apk del --purge .build-deps \
    && rm -rf /app/*

# add local files
COPY dist/root/ /

# expose ports
EXPOSE 1080

