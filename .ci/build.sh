#!/bin/sh
# Compatible with GitLab registry
# Should be run under Alpine Linux CI/CD
set -euo pipefail

echo " --- Building |MAIN| --- "
docker pull "${CONTAINER_IMAGE}:latest" || true
docker build --cache-from "${CONTAINER_IMAGE}:latest" -t "${CI_COMMIT_SHA}" .

echo " --- Tagging --- "
TAG=$(docker inspect -f '{{ .Config.Labels.version }}' "${CI_COMMIT_SHA}" 2>/dev/null)
docker tag "${CI_COMMIT_SHA}" "${CONTAINER_IMAGE}:${TAG}"
docker tag "${CI_COMMIT_SHA}" "${CONTAINER_IMAGE}:latest"

if [ "${CI_BUILD_REF_SLUG:-}" = "${PUSH_BRANCH:-}" ]; then
	echo " --- Pushing --- "
	docker push "${CONTAINER_IMAGE}:${TAG}"
	docker push "${CONTAINER_IMAGE}:latest"
else
	echo "Branch != ${PUSH_BRANCH}, not pushing..."
fi
